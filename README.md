Credit to [Gabriel Tanner's Article](https://gabrieltanner.org/blog/dicord-music-bot) for the starter code for this project.

# D&D Music Bot

A simple discord music bot designed for playing atmospheric sounds for remote
D&D sessions.

# Setup

1. Clone this repo.

2. Install [node and npm](https://nodejs.org/en/). (check that you did it correctly by opening a terminal and running `npm -v`).

3. Create a discord bot under your account:

   1. Visit the [discord developer portal](https://discordapp.com/developers/applications/) and click **New Application**
   2. Give it a name (like "dnd-music-bot") and click **Create**.
   3. Click the **Bot** tab in the nav on the left and click **Add Bot**.

4. Add the bot to your server:

   1. Click the **OAuth2** tab in your application portal and check the **bot** box in the **scopes** section.
   2. Select **Send Messages**, **Manage Messages**, **Connect**, and **Speak** in the **bot permissions** section.
   3. Copy the generated URL and paste it into your browser.
   4. Select your server from the list and click **Authorize**.

5. Use your bots token:

   1. Go back to your application portal's **Bot** tab.
   2. Copy the **Token** listed under the username.
   3. Paste it into [config.json](config.json) as the `token`:
      ```json
      {
        ...
        "token": "ACDEFhijklmnopQRSTUVwxyz",
        ...
      }
      ```

# Usage

## Running the bot

```sh
$ cd dnd-music-bot
$ npm i   # Only need to do this once
$ npm start
```

Stop the bot with Ctrl+C.

## Using it

With the bot running in the terminal window:

1. Join a voice channel.
2. Type `!list` to list prepared music files (see the **Config** section).  
   (tl;dr: put sound files in a subdirectory called `music/` )
3. Type `!play <name>` to play from that list.
4. Type `!play <url>` to play from a YouTube video.

## Additional commands

| command             | effect                                                                                                 |
| ------------------- | ------------------------------------------------------------------------------------------------------ |
| `!play <url\|file>` | plays from prepared list or a YouTube URL                                                              |
| `!list`             | lists names of files in `config.json["tracks"]`<br>and any files in `music/` that aren't in the config |
| `!volume <volume>`  | changes the volume (0-100)                                                                             |
| `!help`             | list commands                                                                                          |
| `!join`             | join the current voice channel                                                                         |
| `!leave`            | leave the current voice channel                                                                        |

## Config

### config.json

| key                            |                                              |
| ------------------------------ | -------------------------------------------- |
| `prefix` (default: `"!"`)      | (string) prefix for commands                 |
| `token`                        | (string) bot token from application page     |
| `startVolume` (default: `100`) | (number) volume to play when first joining   |
| `tracks`                       | (array of track descriptions) see **Tracks** |

### Tracks

The prepared tracklist (shown by `!list`) is determined by:

1. Any files found in a subdirectory called `./music`, and
1. The `tracks` array in `config.json`.

   Example:

   ```json
   // config.json
   {
     // ...
     "tracks": [
       {
         "name": "mylocalsound",
         "file": "music/path.mp4",
         "volumeMultiplier": 2.0
       },
       {
         "name": "anothersound",
         "file": "another/path.ogg"
         // default volumeMultiplier is 1.0
       },
       {
         "name": "myurlsound",
         "url": "https://www.youtube.com/watch?v=oHg5SJYRHA0",
         "volumeMultiplier": 2.0
       }
     ]
     // ...
   }
   ```

If a track file is found in `music/` _and_ has an entry in the custom track
config, it will only be listed once with the correct properties.
