import Discord, { StreamDispatcher, VoiceConnection } from "discord.js";
import { createReadStream } from "fs";
import { FFmpeg } from "prism-media"; // @todo(mike) remove this dependency it's just a ffmpeg wrapper
import { Readable } from "stream";
import ytdl from "ytdl-core";
import FadeTransform from "./fade-transform";
import MusicBot, { Song } from "./music-bot";

const commands = ["play", "list", "volume", "help", "join", "leave"] as const;

/** Returns a bulleted list of all the string in arr */
export function bulleted(arr: readonly string[], sort = false): string {
  let m = arr.map((s) => `  • ${s}`);
  if (sort) m = m.sort();
  return m.join("\n");
}

class GuildSession {
  bot;
  guild;
  prefix;
  volume;
  currentFader: FadeTransform | null = null;
  streamsToCleanup: Readable[] = [];
  connection: VoiceConnection | null = null;
  dispatcher: StreamDispatcher | null = null;

  constructor(
    bot: MusicBot,
    guild: Discord.Guild,
    prefix: string,
    startVolume: number
  ) {
    this.bot = bot;
    this.guild = guild;
    this.prefix = prefix;
    this.volume = startVolume;
  }

  async send(msg: Discord.Message, body: any): Promise<any>;
  async send(msg: Discord.Message, title: any, body: any): Promise<any>;
  async send(msg: Discord.Message, title: any, body?: any): Promise<any> {
    try {
      if (body !== undefined) {
        return await msg.channel.send(
          new Discord.MessageEmbed().setTitle(title).setDescription(body)
        );
      } else {
        return await msg.channel.send(
          new Discord.MessageEmbed().setDescription(title)
        );
      }
    } catch (e) {
      return await msg.channel.send(`**${title}**\n${body}`);
    }
  }

  async sendError(msg: Discord.Message, body: any): Promise<any>;
  async sendError(msg: Discord.Message, title: any, body: any): Promise<any>;
  async sendError(msg: Discord.Message, title: any, body?: any): Promise<any> {
    try {
      if (body !== undefined) {
        return await msg.channel.send(
          new Discord.MessageEmbed()
            .setColor("#FF5733")
            .setTitle(title)
            .setDescription(body)
        );
      } else {
        return await msg.channel.send(
          new Discord.MessageEmbed().setColor("#FF5733").setDescription(title)
        );
      }
    } catch (e) {
      return await msg.channel.send(`**${title}**\n${body}`);
    }
  }

  async handleMessage(msg: Discord.Message) {
    if (msg.author.bot) return;
    if (!msg.content.startsWith(this.prefix)) return;
    const [cmdStart, arg] = msg.content.split(/\s+(.+)/);

    if (cmdStart === this.prefix) {
      return await this.handleHelp(msg);
    }

    const possibilities = commands.filter((maybe) =>
      maybe.startsWith(cmdStart.substr(1))
    );

    if (possibilities.length === 0) {
      this.sendError(msg, "Invalid command!");
      return await this.handleHelp(msg);
    } else if (possibilities.length === 1) {
      const cmd = possibilities[0];

      console.log(
        `${msg.author.username} (${this.guild.name}): ${msg.content}`
      );

      try {
        switch (cmd) {
          case "play":
            return await this.handlePlay(msg, arg);
          case "list":
            return await this.handleList(msg);
          case "volume":
            return await this.handleVolume(msg, arg);
          case "help":
            return await this.handleHelp(msg);
          case "join":
            return await this.join(msg);
          case "leave":
            return this.leave();
        }
      } catch (e) {
        return await this.sendError(msg, e);
      }
    } else {
      return await this.send(
        msg,
        `Ambiguous command. Did you mean:\n${bulleted(possibilities, true)}`
      );
    }
  }

  async handlePlay(msg: Discord.Message, arg: string) {
    if (!arg) throw new Error("Please provide a URL or a track name");

    // disambiguate song title @todo(mike) combine with _songFromArg?
    const possibilities = [...this.bot.fileSongs, ...this.bot.urlSongs]
      .filter((s) => s.title.startsWith(arg))
      .map((s) => s.title);
    if (possibilities.length === 0) {
      return this.send(
        msg,
        `Song not found. Possible songs:\n${this.bot.trackList()}`
      );
    } else if (possibilities.length > 1) {
      return this.send(
        msg,
        `Song title is ambiguous. Did you mean:\n${bulleted(
          possibilities,
          true
        )}`
      );
    }

    const song = await this._songFromArg(msg, arg);
    this.send(msg, `Playing ${song.title}...`);

    await this.join(msg);

    const fader = new FadeTransform({ volume: 0 });

    const stream = this._getConvertedStream(song);
    this.streamsToCleanup.push(stream);

    stream.pipe(fader);

    let oldFader: FadeTransform | null = null;
    if (this.currentFader) {
      oldFader = this.currentFader;
      await oldFader.setVolume(0);
      oldFader.end();
      // @todo(mike) destroy oldFader?
    }

    if (this.dispatcher) {
      const oldDispatcher = this.dispatcher;
      await new Promise((resolve) => {
        oldDispatcher.once("finish", resolve);
      });
      oldFader?.destroy();
    }

    this.currentFader = fader;

    this.currentFader.once("finish", () => {
      if (this.currentFader == fader) {
        this.currentFader.destroy();
        this.currentFader = null;
      }
    });

    const newDispatcher =
      this.connection?.play(fader, {
        type: "converted",
      }) || null;

    this.dispatcher = newDispatcher;

    this.dispatcher?.once("finish", () => {
      if (this.dispatcher === newDispatcher) {
        this.dispatcher = null;
      }
    });

    await fader.setVolume(this.volume * (song.volumeMultiplier ?? 1.0));
  }

  async handleList(msg: Discord.Message) {
    await this.send(
      msg,
      `Play one of the following with !play <name>:\n${this.bot.trackList()}`
    );
  }

  async handleVolume(msg: Discord.Message, arg?: string) {
    const vol = Number(arg);
    if (isNaN(vol) || vol < 0) {
      await this.send(msg, `Current volume: ${this.volume * 100}`);
    } else {
      this.volume = vol / 100;
      this.currentFader?.setVolume(this.volume);
      await this.send(msg, `Setting volume to ${vol}`);
    }
  }

  async handleHelp(msg: Discord.Message) {
    return this.send(msg, `Possible commands:\n${bulleted(commands)}`);
  }

  async join(msg: Discord.Message) {
    const voiceChannel = msg.member?.voice.channel;
    if (!voiceChannel) {
      throw new Error("You need to join a voice channel first!");
    }
    if (!this.connection) {
      this.connection = (await msg.member?.voice.channel?.join()) ?? null;
    }
  }

  leave() {
    if (this.currentFader) {
      this.currentFader.destroy();
      this.currentFader = null;
    }
    if (this.dispatcher) {
      this.dispatcher.destroy();
      this.dispatcher = null;
    }
    if (this.connection) {
      this.connection.channel.leave();
      this.connection = null;
    }
  }

  private async _songFromArg(msg: Discord.Message, arg: string) {
    let song: Song | undefined = this.bot.fileSongs.find((t) =>
      t.title.startsWith(arg)
    );

    if (!song) {
      let url = this.bot.urlSongs.find((val) => val.title.startsWith(arg))?.url;
      if (!url) url = arg;

      try {
        const info = await ytdl.getBasicInfo(url);
        song = {
          title: info.videoDetails.title,
          url: info.videoDetails.video_url,
        };
      } catch (e) {
        throw new Error("could not get info for URL");
      }
    }

    return song;
  }

  private _getConvertedStream(song: Song) {
    let stream: Readable;
    if (song.url) {
      stream = ytdl(song.url, {
        filter: "audioonly",
        quality: "highestaudio",
      });
      stream;
    } else if (song.file) {
      stream = createReadStream(song.file);
    } else {
      throw new Error("could not play song");
    }

    const transcoder = new FFmpeg({
      // prettier-ignore
      args: [
        "-analyzeduration", "0",
        "-loglevel", "0",
        "-f", "s16le",
        "-ar", "48000",
        "-ac", "2",
      ],
    });

    this.streamsToCleanup.push(stream);
    this.streamsToCleanup.push(transcoder);

    stream.pipe(transcoder);

    return transcoder;
  }

  destroy() {
    if (this.dispatcher) {
      this.dispatcher.end();
      this.dispatcher.destroy();
    }
    this.connection?.disconnect();
    this.streamsToCleanup.forEach((s) => s.destroy());
    this.currentFader?.destroy();
  }
}

export default GuildSession;
