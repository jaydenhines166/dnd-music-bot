import Discord from "discord.js";
import GuildSession from "./guild-session";
import path from "path";

export interface Song {
  title: string;
  url?: string;
  file?: string;
  volumeMultiplier?: number;
}

interface FileSong extends Song {
  file: string;
}

interface UrlSong extends Song {
  url: string;
}

/**
 * Bot that creates session objects for guilds and forwards messages to those
 * sessions.
 */
class MusicBot {
  readonly client = new Discord.Client();
  private guilds = new Map<string, GuildSession>();

  readonly prefix;
  readonly token;
  readonly startVolume;
  readonly fileSongs;
  readonly urlSongs;

  constructor(
    prefix: string,
    token: string,
    startVolume: number,
    fileSongs: FileSong[],
    urlSong: UrlSong[]
  ) {
    this.prefix = prefix;
    this.token = token;
    this.startVolume = startVolume;
    this.fileSongs = fileSongs;
    this.urlSongs = urlSong;

    console.log("Prepared files:");
    console.log(this.trackList(true));

    this.client.on("message", (msg) => {
      if (msg.guild === null) return;

      if (!this.guilds.has(msg.guild.id)) {
        console.log(`Creating guild ${msg.guild.name}`);
        this.guilds.set(
          msg.guild.id,
          new GuildSession(this, msg.guild, prefix, startVolume)
        );
      }

      this.guilds.get(msg.guild.id)?.handleMessage(msg);
    });

    this.client.on("guildDelete", (guild) => {
      if (this.guilds.has(guild.id)) {
        console.log(`Removing guild ${guild.name}`);
        this.guilds.get(guild.id)!.destroy();
        this.guilds.delete(guild.id);
      }
    });

    this.client.login(this.token);
  }

  /**
   * Returns string list of tracknames, optionally with the URL/file shown
   */
  trackList(showSource = false) {
    return [...this.fileSongs, ...this.urlSongs]
      .map(
        (t: Song) =>
          `  • ${t.title}${
            showSource ? `\t(${t.url ?? path.relative(".", t.file!)})` : ""
          }`
      )
      .sort()
      .join("\n");
  }

  destroy() {
    this.guilds.forEach((session) => session.destroy());
    this.guilds.clear();
    this.client.destroy();
  }
}

export default MusicBot;
